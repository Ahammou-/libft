/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsub.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ahammou- <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/21 16:31:31 by ahammou-          #+#    #+#             */
/*   Updated: 2018/10/24 11:14:19 by ahammou-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strsub(char const *s, unsigned int start, size_t len)
{
	char	*ps;

	if (s == NULL)
		return (NULL);
	if (!(ps = ft_strnew(len)))
		return (NULL);
	ft_strcpy(ps, s + start);
	ps[len] = '\0';
	return (ps);
}
